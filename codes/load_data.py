import os
import re
import json
import glob
from collections import defaultdict

import numpy as np
import pandas as pd

from logging import getLogger

TRAIN_DATA = '../input/train.csv'
TRAIN_FILES = glob.glob("../input/train/*.json")
TEST_FILES = glob.glob("../input/test/*.json")

logger = getLogger(__name__)

def read_csv(path):
    logger.debug('enter')
    df = pd.read_csv(path)
    logger.debug('exit')
    return df

def load_train_data():
    logger.debug('enter')
    df = read_csv(TRAIN_DATA)
    logger.debug('exit')
    return df

def load_train_json():
    df_train_publications = pd.DataFrame()

    for train_file in TRAIN_FILES:
        file_data = pd.read_json(train_file)
        file_data.insert(0, 'pub_id', train_file.split('/')[-1].split('.')[0])
        df_train_publications = pd.concat([df_train_publications, file_data])

    df_train_publications.to_csv('df_train_publications.csv', index=False)
    return df_train_publications

def load_test_json():
    df_test_publications = pd.DataFrame()

    for test_file in TEST_FILES:
        file_data = pd.read_json(test_file)
        file_data.insert(0, 'pub_id', test_file.split('/')[-1].split('.')[0])
        df_test_publications = pd.concat([df_test_publications, file_data])

    df_test_publications.to_csv('df_test_publications.csv', index=False)
    return df_test_publications

if __name__ == '__main__':
    print(load_train_data().head)
    print(load_train_json().head)
    print(load_test_json().head)
